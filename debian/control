Source: fence-agents
Section: admin
Priority: optional
Maintainer: Debian HA Maintainers <debian-ha-maintainers@alioth-lists.debian.net>
Uploaders: Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
           Valentin Vidic <vvidic@debian.org>
Build-Depends: autoconf,
               automake,
               bison,
               debhelper-compat (= 13),
               dh-python,
               flex,
               iputils-ping,
               libcpg-dev,
               libglib2.0-dev,
               libnet-telnet-perl,
               libnspr4-dev,
               libnss3-dev,
               libtool,
               libvirt-dev,
               libxml2-dev,
               libxml2-utils,
               perl,
               python3,
               python3-boto3,
               python3-googleapi,
               python3-kubernetes,
               python3-oauth2client,
               python3-pexpect,
               python3-pycurl,
               python3-requests,
               python3-suds,
               time,
               uuid-dev,
               xsltproc,
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://github.com/ClusterLabs/fence-agents
Vcs-Browser: https://salsa.debian.org/ha-team/fence-agents
Vcs-Git: https://salsa.debian.org/ha-team/fence-agents.git

Package: fence-agents
Architecture: any
Depends: ${misc:Depends}, ${perl:Depends}, ${python3:Depends}, ${shlibs:Depends},
         python3-boto3,
         python3-googleapi,
         python3-oauth2client,
         python3-pycurl,
         python3-pexpect,
         python3-requests,
         python3-suds
Breaks: cman (<= 3.0.12-2ubuntu4)
Replaces: cman (<= 3.0.12-2ubuntu4)
Recommends: libnet-telnet-perl,
            openssh-client,
            sg3-utils,
            snmp
Suggests: fence-virt,
# Needed by fence_azure_arm:
          python3-adal,
          python3-azure,
# Needed by fence_openstack:
          python3-keystoneauth1,
          python3-keystoneclient,
          python3-novaclient,
# Needed by fence_kubevirt:
          python3-kubernetes,
Description: Fence Agents for Red Hat Cluster
 Red Hat Fence Agents is a collection of scripts to handle remote
 power management for several devices.  They allow failed or
 unreachable nodes to be forcibly restarted and removed from the
 cluster.
 .
 Fence agents included:
 ${agents}

Package: fence-virt
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Pluggable fencing framework for virtual machines - agent
 The fencing framework consists of the agent (fence_virt) and
 the host daemon (fence_virtd). The fence_virtd host daemon is
 responsible for processing fencing requests from fence_virt agents
 running in virtual machines and routing the requests to the
 appropriate physical machine for action.
 .
 This package contains the fence_virt agent to be used inside the
 virtual machine.

Package: fence-virtd
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Pre-Depends: ${misc:Pre-Depends}
Description: Pluggable fencing framework for virtual machines - daemon
 The fencing framework consists of the agent (fence_virt) and
 the host daemon (fence_virtd). The fence_virtd host daemon is
 responsible for processing fencing requests from fence_virt agents
 running in virtual machines and routing the requests to the
 appropriate physical machine for action.
 .
 This package contains the fence_virtd daemon to be used on the
 physical host.
